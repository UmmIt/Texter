﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spam_Text
{
    public partial class TenRows : Form
    {
        public TenRows()
        {
            InitializeComponent();
        }

        [DllImport("user32.dll")]
        public static extern int GetAsyncKeyState(Keys vKeys);

        private void TenRows_Load(object sender, EventArgs e)
        {
            timer1.Interval = 3000;
            textBox1.ForeColor = Color.RoyalBlue;
            textBox2.ForeColor = Color.RoyalBlue;
            textBox3.ForeColor = Color.RoyalBlue;
            textBox4.ForeColor = Color.RoyalBlue;
            textBox5.ForeColor = Color.RoyalBlue;
            textBox6.ForeColor = Color.RoyalBlue;
            textBox7.ForeColor = Color.RoyalBlue;
            textBox8.ForeColor = Color.RoyalBlue;
            textBox9.ForeColor = Color.RoyalBlue;
        }

        private void HotKey_Pressed(object sender, HotKeyEventArgs e)
        {
            if (e.Modifiers == KeyModifiers.None)
            {
                if (e.Key == Keys.F3)
                {
                    timer1.Enabled = true;
                    label11.Text = "狀態 : 運行中";
                }

                if (e.Key == Keys.F4)
                {
                    timer1.Enabled = false;
                    label11.Text = "狀態 : 停止中";
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            MessageBox.Show("1S", "秒數");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            timer1.Interval = 2000;
            MessageBox.Show("2S", "秒數");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            timer1.Interval = 3000;
            MessageBox.Show("3S", "秒數");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            timer1.Interval = 4000;
            MessageBox.Show("4S", "秒數");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            timer1.Interval = 5000;
            MessageBox.Show("5S", "秒數");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            MessageBox.Show("Text Clear ! !", "Clear");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (GetAsyncKeyState(Keys.F3) == -32767)
            {
                timer1.Enabled = false;
                MessageBox.Show("已停止洗頻 !!", "停止 (Stop)");
            }

            if (textBox1.Text == "")
            {
                timer1.Enabled = false;
                MessageBox.Show("不能空白 ! !", "Error");
            }

            if (textBox1.Text == "" && textBox2.Text == "")
            {
                timer1.Enabled = false;
                MessageBox.Show("不能空白 ! !", "Error");
            }

            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "")
            {
                timer1.Enabled = false;
                MessageBox.Show("不能空白 ! !", "Error");
            }

            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "" && textBox4.Text == "")
            {
                timer1.Enabled = false;
                MessageBox.Show("不能空白 ! !", "Error");
            }

            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "" && textBox4.Text == "" && textBox5.Text == "")
            {
                timer1.Enabled = false;
                MessageBox.Show("不能空白 ! !", "Error");
            }

            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "" && textBox4.Text == "" && textBox5.Text == "" &&
                textBox6.Text == "")
            {
                timer1.Enabled = false;
                MessageBox.Show("不能空白 ! !", "Error");
            }

            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "" && textBox4.Text == "" && textBox5.Text == "" &&
                textBox6.Text == "" && textBox7.Text == "")
            {
                timer1.Enabled = false;
                MessageBox.Show("不能空白 ! !", "Error");
            }

            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "" && textBox4.Text == "" && textBox5.Text == "" &&
                textBox6.Text == "" && textBox7.Text == "" && textBox8.Text == "")
            {
                timer1.Enabled = false;
                MessageBox.Show("不能空白 ! !", "Error");
            }

            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "" && textBox4.Text == "" && textBox5.Text == "" &&
                textBox6.Text == "" && textBox7.Text == "" && textBox8.Text == "" && textBox9.Text == "")
            {
                timer1.Enabled = false;
                MessageBox.Show("不能空白 ! !", "Error");
            }

            else
            {
                SendKeys.Send(textBox1.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox2.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox3.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox4.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox5.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox6.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox7.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox8.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox9.Text);
                SendKeys.Send("{Enter}");
            }
        }
    }
}
