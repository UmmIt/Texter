﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Spam_Text
{
    public partial class Main : Form
    {
        private string blank = "";

        public Main()
        {
            InitializeComponent();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (var process in Process.GetProcessesByName("Spam Text"))
            {
                process.Kill();
            }
        }

        [DllImport("user32.dll")]
        public static extern int GetAsyncKeyState(Keys vKeys);

        private void Main_Load(object sender, EventArgs e)
        {
            HotKeyManager.RegisterHotKey(Keys.F1, KeyModifiers.None);
            HotKeyManager.RegisterHotKey(Keys.F2, KeyModifiers.None);
            HotKeyManager.RegisterHotKey(Keys.F3, KeyModifiers.None);
            HotKeyManager.RegisterHotKey(Keys.F4, KeyModifiers.None);
            HotKeyManager.HotKeyPressed += new EventHandler<HotKeyEventArgs>(HotKey_Pressed);
            MessageBox.Show("歡迎你使用 Unknown 洗頻器 ! !", "Unknown 洗頻器 Version 3.0");

            // var About = new About();
            // About.Show();
            textBox1.Visible = true;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
        }

        private void HotKey_Pressed(object sender, HotKeyEventArgs e)
        {
            if (e.Modifiers == KeyModifiers.None)
            {
                if (e.Key == Keys.F1)
                {
                    timer1.Enabled = true;
                    label9.Text = "狀態 : 運行中";
                    label9.ForeColor = System.Drawing.Color.DarkRed;
                }

                if (e.Key == Keys.F2)
                {
                    timer1.Enabled = false;
                    label9.Text = "狀態 : 停止中";
                    label9.ForeColor = System.Drawing.Color.Black;
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (GetAsyncKeyState(Keys.F3) == -32767)
            {
                timer1.Enabled = false;
                MessageBox.Show("已停止洗頻 !!", "停止 (Stop)");
            }

            if (textBox2.Visible == false && textBox3.Visible == false && textBox4.Visible == false && textBox5.Visible == false)
            {
                SendKeys.Send(textBox1.Text);
                SendKeys.Send("{Enter}");
            }

            if (textBox2.Visible == true && textBox3.Visible == false && textBox4.Visible == false && textBox5.Visible == false)
            {
                SendKeys.Send(textBox1.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox2.Text);
                SendKeys.Send("{Enter}");
            }

            if (textBox2.Visible == true && textBox3.Visible == true && textBox4.Visible == false && textBox5.Visible == false)
            {
                SendKeys.Send(textBox1.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox2.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox3.Text);
                SendKeys.Send("{Enter}");
            }

            if (textBox2.Visible == true && textBox3.Visible == true && textBox4.Visible == true && textBox5.Visible == false)
            {
                SendKeys.Send(textBox1.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox2.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox3.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox4.Text);
                SendKeys.Send("{Enter}");
            }

            if (textBox2.Visible == true && textBox3.Visible == true && textBox4.Visible == true && textBox5.Visible == true)
            {
                SendKeys.Send(textBox1.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox2.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox3.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox4.Text);
                SendKeys.Send("{Enter}");
                SendKeys.Send(textBox5.Text);
                SendKeys.Send("{Enter}");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = blank;
            textBox2.Text = blank;
            textBox3.Text = blank;
            textBox4.Text = blank;
            textBox5.Text = blank;
            MessageBox.Show("已清理所有輸入的文字 ! !", "清理 (Clean)");
        }

        private void 秒預設DefultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            MessageBox.Show("已反回預設速度 (Default) 1.0 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Interval = 900;
            MessageBox.Show("已反回預設速度 (Default) 0.9 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            timer1.Interval = 800;
            MessageBox.Show("已反回預設速度 (Default) 0.8 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            timer1.Interval = 700;
            MessageBox.Show("已反回預設速度 (Default) 0.7 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            timer1.Interval = 600;
            MessageBox.Show("已反回預設速度 (Default) 0.6 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            timer1.Interval = 500;
            MessageBox.Show("已反回預設速度 (Default) 0.5 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            timer1.Interval = 400;
            MessageBox.Show("已反回預設速度 (Default) 0.4 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            timer1.Interval = 300;
            MessageBox.Show("已反回預設速度 (Default) 0.3 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            timer1.Interval = 200;
            MessageBox.Show("已反回預設速度 (Default) 0.2 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            timer1.Interval = 100;
            MessageBox.Show("已反回預設速度 (Default) 0.1 秒 !!", "速度設置");
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            timer1.Interval = 2000;
            MessageBox.Show("已設定速度為 2.0 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem28_Click(object sender, EventArgs e)
        {
            timer1.Interval = 3000;
            MessageBox.Show("已設定速度為 3.0 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem29_Click(object sender, EventArgs e)
        {
            timer1.Interval = 4000;
            MessageBox.Show("已設定速度為 4.0 秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem30_Click(object sender, EventArgs e)
        {
            timer1.Interval = 5000;
            MessageBox.Show("已設定速度為 5.0 秒 !!", "速度設置");
        }

        private void 一行預設DefaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            textBox2.Text = blank;
            textBox3.Text = blank;
            textBox4.Text = blank;
            textBox5.Text = blank;
            MessageBox.Show("已反回預設行數 (Rows) 一行 !!", "行數設置 (Rows Setting)");
        }

        private void 二行TwoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            label3.Visible = true;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            textBox3.Text = blank;
            textBox4.Text = blank;
            textBox5.Text = blank;
            MessageBox.Show("已設定行數為二行 !!", "行數設置 (Rows Setting)");
        }

        private void 三行ThreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            textBox4.Visible = false;
            textBox5.Visible = false;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = false;
            label6.Visible = false;
            textBox4.Text = blank;
            textBox5.Text = blank;
            MessageBox.Show("已設定行數為三行 !!", "行數設置 (Rows Setting)");
        }

        private void 四行FourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            textBox4.Visible = true;
            textBox5.Visible = false;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            label6.Visible = false;
            textBox5.Text = blank;
            MessageBox.Show("已設定行數為四行 !!", "行數設置 (Rows Setting)");
        }

        private void 五行FiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            textBox4.Visible = true;
            textBox5.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            label6.Visible = true;
            MessageBox.Show("已設定行數為五行 !!", "行數設置 (Rows Setting)");
        }

        private void 十行ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coding ...", "Coding");
            var TenRows = new TenRows();
            TenRows.Show();
        }

        private void 秒ToolStripMenuItem17_Click(object sender, EventArgs e)
        {
            timer1.Interval = 100;
            MessageBox.Show("已設定速度為 0.010秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            timer1.Interval = 90;
            MessageBox.Show("你設定速度為 0.09秒 !!", "速度設置");
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            timer1.Interval = 80;
            MessageBox.Show("已設定速度為 0.08秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            timer1.Interval = 70;
            MessageBox.Show("已設定速度為 0.07秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            timer1.Interval = 60;
            MessageBox.Show("已設定速度為 0.06秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            timer1.Interval = 50;
            MessageBox.Show("已設定速度為 0.05秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem13_Click(object sender, EventArgs e)
        {
            timer1.Interval = 40;
            MessageBox.Show("已設定速度為 0.04秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem14_Click(object sender, EventArgs e)
        {
            timer1.Interval = 30;
            MessageBox.Show("已設定速度為 0.03秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem15_Click(object sender, EventArgs e)
        {
            timer1.Interval = 20;
            MessageBox.Show("已設定速度為 0.02秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem16_Click(object sender, EventArgs e)
        {
            timer1.Interval = 10;
            MessageBox.Show("已設定速度為 0.01秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem18_Click(object sender, EventArgs e)
        {
            timer1.Interval = 10;
            MessageBox.Show("已設定速度為 0.0010秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem19_Click(object sender, EventArgs e)
        {
            timer1.Interval = 9;
            MessageBox.Show("已設定速度為 0.009秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem20_Click(object sender, EventArgs e)
        {
            timer1.Interval = 8;
            MessageBox.Show("已設定速度為 0.008秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem21_Click(object sender, EventArgs e)
        {
            timer1.Interval = 7;
            MessageBox.Show("已設定速度為 0.007秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem22_Click(object sender, EventArgs e)
        {
            timer1.Interval = 6;
            MessageBox.Show("已設定速度為 0.006秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem23_Click(object sender, EventArgs e)
        {
            timer1.Interval = 5;
            MessageBox.Show("已設定速度為 0.005秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem24_Click(object sender, EventArgs e)
        {
            timer1.Interval = 4;
            MessageBox.Show("已設定速度為 0.004秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem25_Click(object sender, EventArgs e)
        {
            timer1.Interval = 3;
            MessageBox.Show("已設定速度為 0.003秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem26_Click(object sender, EventArgs e)
        {
            timer1.Interval = 2;
            MessageBox.Show("已設定速度為 0.002秒 !!", "速度設置");
        }

        private void 秒ToolStripMenuItem27_Click(object sender, EventArgs e)
        {
            timer1.Interval = 1;
            MessageBox.Show("已設定速度為 0.001秒 !!", "速度設置");
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("你確定要關閉本程式嗎?", "Close?", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void 預設DefaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Text = blank;
            textBox2.Text = blank;
            textBox3.Text = blank;
            textBox4.Text = blank;
            textBox5.Text = blank;
            label2.Visible = true;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            textBox1.Visible = true;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            textBox1.ForeColor = Color.RoyalBlue;
            textBox2.ForeColor = Color.RoyalBlue;
            textBox3.ForeColor = Color.RoyalBlue;
            textBox4.ForeColor = Color.RoyalBlue;
            textBox5.ForeColor = Color.RoyalBlue;
            MessageBox.Show("行數已反回預設 (Default) 1行", "行數預設 (Default)");
            MessageBox.Show("輸入的文字已經反回預 (Default) RoyalBlue", "輸入的文字預設 (Default)");
            MessageBox.Show("全部輸入的文字已反回預設(為0文字)", "文字預設 (Default)");
            timer1.Interval = 1000;
            MessageBox.Show("秒數已反回預設 (Default) 1秒", "秒數預設 (Default)");
            label1.ForeColor = Color.RoyalBlue;
            label2.ForeColor = Color.RoyalBlue;
            label3.ForeColor = Color.RoyalBlue;
            label4.ForeColor = Color.RoyalBlue;
            label5.ForeColor = Color.RoyalBlue;
            label6.ForeColor = Color.RoyalBlue;
            label7.ForeColor = Color.RoyalBlue;
            button1.ForeColor = Color.RoyalBlue;
            button2.ForeColor = Color.RoyalBlue;
            button3.ForeColor = Color.RoyalBlue;
            秒數SecondToolStripMenuItem.ForeColor = Color.RoyalBlue;
            秒預設DefultToolStripMenuItem.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem28.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem29.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem30.ForeColor = Color.RoyalBlue;
            toolStripMenuItem5.ForeColor = Color.RoyalBlue;
            速度設置20秒50秒最低速度ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem1.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem2.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem3.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem4.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem5.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem6.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem7.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem8.ForeColor = Color.RoyalBlue;
            秒數Seconds009001秒ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem17.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem9.ForeColor = Color.RoyalBlue;
            toolStripMenuItem2.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem10.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem11.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem12.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem13.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem14.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem15.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem16.ForeColor = Color.RoyalBlue;
            秒數Seconds000100001秒高速度ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem18.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem19.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem20.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem21.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem22.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem23.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem24.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem25.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem26.ForeColor = Color.RoyalBlue;
            秒ToolStripMenuItem27.ForeColor = Color.RoyalBlue;
            行數ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            一行預設DefaultToolStripMenuItem.ForeColor = Color.RoyalBlue;
            二行TwoToolStripMenuItem.ForeColor = Color.RoyalBlue;
            三行ThreeToolStripMenuItem.ForeColor = Color.RoyalBlue;
            四行FourToolStripMenuItem.ForeColor = Color.RoyalBlue;
            五行FiveToolStripMenuItem.ForeColor = Color.RoyalBlue;
            十行ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            資訊ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            作者資訊ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            預設DefaultToolStripMenuItem.ForeColor = Color.RoyalBlue;
            其他設置OtherSettingToolStripMenuItem.ForeColor = Color.RoyalBlue;
            一鍵回預設ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            顏色設置ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            背景顏色設置ToolStripMenuItem.ForeColor = Color.RoyalBlue;
            MessageBox.Show("全部文字顏色已反回預設顏色", "顏色預設 (Default)");
            this.BackColor = SystemColors.Control;
            label1.BackColor = SystemColors.Control;
            label2.BackColor = SystemColors.Control;
            label3.BackColor = SystemColors.Control;
            label4.BackColor = SystemColors.Control;
            label5.BackColor = SystemColors.Control;
            label6.BackColor = SystemColors.Control;
            label7.BackColor = SystemColors.Control;
            textBox1.BackColor = SystemColors.Control;
            textBox2.BackColor = SystemColors.Control;
            textBox3.BackColor = SystemColors.Control;
            textBox4.BackColor = SystemColors.Control;
            textBox5.BackColor = SystemColors.Control;
            button1.BackColor = SystemColors.Control;
            button2.BackColor = SystemColors.Control;
            button3.BackColor = SystemColors.Control;
            menuStrip1.BackColor = SystemColors.Control;
            秒數SecondToolStripMenuItem.BackColor = SystemColors.Control;
            秒預設DefultToolStripMenuItem.BackColor = SystemColors.Control;
            秒ToolStripMenuItem28.BackColor = SystemColors.Control;
            秒ToolStripMenuItem29.BackColor = SystemColors.Control;
            秒ToolStripMenuItem30.BackColor = SystemColors.Control;
            toolStripMenuItem5.BackColor = SystemColors.Control;
            速度設置20秒50秒最低速度ToolStripMenuItem.BackColor = SystemColors.Control;
            秒ToolStripMenuItem.BackColor = SystemColors.Control;
            秒ToolStripMenuItem1.BackColor = SystemColors.Control;
            秒ToolStripMenuItem2.BackColor = SystemColors.Control;
            秒ToolStripMenuItem3.BackColor = SystemColors.Control;
            秒ToolStripMenuItem4.BackColor = SystemColors.Control;
            秒ToolStripMenuItem5.BackColor = SystemColors.Control;
            秒ToolStripMenuItem6.BackColor = SystemColors.Control;
            秒ToolStripMenuItem7.BackColor = SystemColors.Control;
            秒ToolStripMenuItem8.BackColor = SystemColors.Control;
            秒數Seconds009001秒ToolStripMenuItem.BackColor = SystemColors.Control;
            秒ToolStripMenuItem17.BackColor = SystemColors.Control;
            秒ToolStripMenuItem9.BackColor = SystemColors.Control;
            toolStripMenuItem2.BackColor = SystemColors.Control;
            秒ToolStripMenuItem10.BackColor = SystemColors.Control;
            秒ToolStripMenuItem11.BackColor = SystemColors.Control;
            秒ToolStripMenuItem12.BackColor = SystemColors.Control;
            秒ToolStripMenuItem13.BackColor = SystemColors.Control;
            秒ToolStripMenuItem14.BackColor = SystemColors.Control;
            秒ToolStripMenuItem15.BackColor = SystemColors.Control;
            秒ToolStripMenuItem16.BackColor = SystemColors.Control;
            秒數Seconds000100001秒高速度ToolStripMenuItem.BackColor = SystemColors.Control;
            秒ToolStripMenuItem18.BackColor = SystemColors.Control;
            秒ToolStripMenuItem19.BackColor = SystemColors.Control;
            秒ToolStripMenuItem20.BackColor = SystemColors.Control;
            秒ToolStripMenuItem21.BackColor = SystemColors.Control;
            秒ToolStripMenuItem22.BackColor = SystemColors.Control;
            秒ToolStripMenuItem23.BackColor = SystemColors.Control;
            秒ToolStripMenuItem24.BackColor = SystemColors.Control;
            秒ToolStripMenuItem25.BackColor = SystemColors.Control;
            秒ToolStripMenuItem26.BackColor = SystemColors.Control;
            秒ToolStripMenuItem27.BackColor = SystemColors.Control;
            行數ToolStripMenuItem.BackColor = SystemColors.Control;
            一行預設DefaultToolStripMenuItem.BackColor = SystemColors.Control;
            二行TwoToolStripMenuItem.BackColor = SystemColors.Control;
            三行ThreeToolStripMenuItem.BackColor = SystemColors.Control;
            四行FourToolStripMenuItem.BackColor = SystemColors.Control;
            五行FiveToolStripMenuItem.BackColor = SystemColors.Control;
            十行ToolStripMenuItem.BackColor = SystemColors.Control;
            資訊ToolStripMenuItem.BackColor = SystemColors.Control;
            作者資訊ToolStripMenuItem.BackColor = SystemColors.Control;
            預設DefaultToolStripMenuItem.BackColor = SystemColors.Control;
            其他設置OtherSettingToolStripMenuItem.BackColor = SystemColors.Control;
            一鍵回預設ToolStripMenuItem.BackColor = SystemColors.Control;
            顏色設置ToolStripMenuItem.BackColor = SystemColors.Control;
            背景顏色設置ToolStripMenuItem.BackColor = SystemColors.Control;
            MessageBox.Show("全部背景顏色已反回預設顏色", "預設 (Default)");
        }

        private void 作者資訊ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var Author = new Author();
            Author.Show();
        }
    }
}