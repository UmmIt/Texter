﻿using System;
using System.Windows.Forms;

namespace Spam_Text
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox2.PasswordChar = '★';
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "Admin") && (textBox2.Text == "admin"))
            {
                MessageBox.Show("Correct !");
                Main haha = new Main();
                haha.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("UserName/Account Or Password / Keys Wrong \nOr Both are wrong", "Wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("Please Try Again", "Wrong", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
